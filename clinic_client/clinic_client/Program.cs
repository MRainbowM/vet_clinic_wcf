﻿using System;

using clinic_client.ClinicServiceRef;
using clinic_service;

namespace clinic_client
{
    class Program
    {
        static ClassesClient methods = new ClassesClient("BasicHttpBinding_IClasses"); 

        public static string token;
        static void Main(string[] args)
        {
            //BasicHttpBinding_IClasses
            
            MainMenu();

        }

        static void MainMenu()
        {
            while (true)
            {
                Console.WriteLine("1 - Авторизация");
                Console.WriteLine("2 - Регистрация");

                string n = Console.ReadLine();
                try
                {
                    int num = Convert.ToInt32(n);
                    switch (num)
                    {
                        case 1:
                            Console.Clear();
                            Console.WriteLine("Авторизация");
                            Authorization();
                            break;
                        case 2:
                            Console.Clear();
                            Console.WriteLine("Регистрация");
                            AddCLient();
                            break;
                        default:
                            Console.WriteLine("Ошибка! Введите 1 или 2");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Введите 1 или 2");
                    continue;
                }
            }
        }

        static void Authorization()
        {
            while (true)
            {
                
                Console.WriteLine("(назад: -1)");
                Console.WriteLine("Введите логин: ");
                string username = Console.ReadLine();
                if (username == "-1") { MainMenu(); Console.Clear(); }
                int user_id = methods.LoginVerification(username);
                if (user_id == 0)
                {
                    Console.WriteLine("Ошибка! неправильный логин");
                    continue;
                }
                Console.Clear();
                while (true)
                {
                    Console.WriteLine("(назад: -1)");
                    Console.WriteLine("Введите пароль: ");
                    string pwd = Console.ReadLine();
                    if (pwd == "-1"){ MainMenu(); Console.Clear(); }
                    token = methods.PassVerification(username, pwd);
                    if (token == "")
                    {
                        Console.WriteLine("Ошибка! неправильный пароль");
                        continue;
                    }
                    else { break; }
                }
                Menu();
            }
        }

        static void AddCLient()
        {
            Console.Clear();
            Console.WriteLine("Введите логин:");
            string username = Console.ReadLine();
            while (true)
            {
                Console.WriteLine("Введите пароль:");
                string pass1 = Console.ReadLine();
                Console.WriteLine("Повторите пароль:");
                string pass2 = Console.ReadLine();
                if (pass1 != pass2)
                {
                    Console.WriteLine("Ошибка! Пароли не совпадают");
                    continue;
                }
                else
                {
                    
                    int id = methods.AddClient(username, pass1);
                    if (id > 0)
                    {
                        Console.Clear();
                        Console.WriteLine("Успех!");
                        MainMenu();
                    }
                }
            }
        }

        static void Menu()
        {
            while (true)
            {
                Console.WriteLine("Главное меню");
                Console.WriteLine("1 - Показать мои приемы");
                Console.WriteLine("2 - Записаться на прием");
                Console.WriteLine("3 - Мои питомцы");
                Console.WriteLine("4 - Выход из ЛК");

                string n = Console.ReadLine();
                try
                {
                    int num = Convert.ToInt32(n);
                    switch (num)
                    {
                        case 1:
                            Console.Clear();
                            GetVisitsByClient();
                            break;
                        case 2:
                            Console.Clear();
                            AddVisit();
                            break;
                        case 3:
                            Console.Clear();
                            MenuPet();
                            break;
                        case 4:
                            MainMenu();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ошибка! Выберите пункт из меню");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Ошибка! Выберите пункт из меню");
                    continue;
                }

            }
           
        }

        static void GetVisitsByClient()
        {
            Console.WriteLine("Мои приемы");
            
            clinic_service.Visit[] visits = methods.GetVisitByClient(token);
            foreach (var visit in visits)
            {
                string visitstr = $"{visit.id} - {visit.date} - {visit.status} - {visit.cabinet} - {visit.filial.address} - {visit.pet.name} - {visit.duration} - {visit.doctor.surname} {visit.doctor.name}";
                Console.WriteLine(visitstr);
            }
        }

        static void AddVisit()
        {
            
            int pet_id = 0;
            int service_id = 0;
            int filial_id = 0;
            int worker_id = 0;
            int schedule_id = 0;
            DateTime date;


            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор питомца");
            int count_pet = GetPets();
            if (count_pet == 0)
            {
                Console.WriteLine("У вас нет зарегистированных питомцев");
                MenuPet();
            }
            while (true)
            {
                Console.WriteLine("Выберите питомца из списка по номеру");
                string n = Console.ReadLine();
                try
                {
                    pet_id = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка!");
                    continue;
                }
                int c = 0;
                clinic_service.Pet[] pets = methods.GetPetByClient(token);
                foreach (var pet in pets)
                {
                    if (pet.id == pet_id)
                    {
                        c = 1;
                        break;
                    }
                }
                if (c == 0)
                {
                    Console.WriteLine("Ошибка!");
                    continue;
                }
                else { break; }
            }

            Console.Clear();
            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор услуги");

            int count_services = GetServices();
            if (count_services == 0)
            {
                Console.Clear();
                Console.WriteLine("Услуги не найдены");
                Menu();
            }
            while (true)
            {
                Console.WriteLine("Выберите услугу из списка по номеру");
                string n = Console.ReadLine();
                try
                {
                    service_id = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Некорректный номер");
                    continue;
                }
                int c = 0;
                clinic_service.Service[] services = methods.GetAllServices();
                foreach (var service in services)
                {
                    if (service.id == service_id)
                    {
                        c = 1;
                        break;
                    }
                }
                if (c == 0)
                {
                    Console.WriteLine("Ошибка! Номера нет в списке");
                    continue;
                }
                else { break; }
            }

            Console.Clear();
            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор филиала");

            int count_filials = GetFilials();
            if (count_filials == 0)
            {
                Console.Clear();
                Console.WriteLine("Филиалы не найдены");
                Menu();
            }

            while (true)
            {
                Console.WriteLine("Выберите филиал из списка по номеру");
                string n = Console.ReadLine();
                try
                {
                    filial_id = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Некорректный номер");
                    continue;
                }
                int c = 0;
                clinic_service.Filial[] filials = methods.GetAllFilials();
                foreach (var filial in filials)
                {
                    if (filial.id == filial_id)
                    {
                        c = 1;
                        break;
                    }
                }
                if (c == 0)
                {
                    Console.WriteLine("Ошибка! Номера нет в списке");
                    continue;
                }
                else { break; }
            }

            Console.Clear();
            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор врача");

            int count_worker = GetWorkersByService(service_id);
            if (count_filials == 0)
            {
                Console.Clear();
                Console.WriteLine("Врачи не найдены");
                Menu();
            }

            while (true)
            {
                Console.WriteLine("Выберите врача из списка по номеру");
                string n = Console.ReadLine();
                try
                {
                    worker_id = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Некорректный номер");
                    continue;
                }
                int c = 0;
                clinic_service.Worker[] workers = methods.GetWorkerByService(service_id);
                foreach (var worker in workers)
                {
                    if (worker.id == worker_id)
                    {
                        c = 1;
                        break;
                    }
                }
                if (c == 0)
                {
                    Console.WriteLine("Ошибка! Номера нет в списке");
                    continue;
                }
                else { break; }
            }

            Console.Clear();
            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор даты");

            int count_dates = GetFreeDates(filial_id, worker_id);
            if (count_dates == 0)
            {
                Console.Clear();
                Console.WriteLine("Даты не найдены");
                Menu();
            }

            while (true)
            {
                Console.WriteLine("Выберите дату из списка по номеру");
                string n = Console.ReadLine();
                try
                {
                    schedule_id = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Некорректный номер");
                    continue;
                }
                int c = 0;
                clinic_service.Schedule[] schedules = methods.GetFreeDateSchedule(filial_id, worker_id);
                foreach (var schedule in schedules)
                {
                    if (schedule.id == schedule_id)
                    {
                        c = 1;
                        break;
                    }
                }
                if (c == 0)
                {
                    Console.WriteLine("Ошибка! Номера нет в списке");
                    continue;
                }
                else { break; }
            }

            Console.Clear();
            Console.WriteLine("Запись на прием");
            Console.WriteLine("Выбор времени");
            int[] services_id = new int[1];
            services_id[0] = service_id;
            int count_times = GetFreeTimes(schedule_id, services_id);
            if (count_times == 0)
            {
                Console.Clear();
                Console.WriteLine("Свободное время не найдено");
                Menu();
            }

            while (true)
            {
                Console.WriteLine("Выберите время из списка по номеру");
                string n = Console.ReadLine();
                int num = 0;
                try
                {
                    num = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Некорректный номер");
                    continue;
                }
                clinic_service.Times[] times = methods.GetFreeTimeSchedule(schedule_id, services_id);
                if (num > times.Length | num <= 0 )
                {
                    Console.WriteLine("Ошибка! Номера нет в списке");
                    continue;
                }
                else
                {
                    date = times[num - 1].start;
                    break; 
                }
            }

            int visit_id = methods.AddVisit(token, worker_id, filial_id, date, pet_id, services_id);

            Console.Clear();
            Console.WriteLine("Успех!");
        }


        static int GetFreeTimes(int schedule_id, params int[] services_id)
        {
            
            clinic_service.Times[] times = methods.GetFreeTimeSchedule(schedule_id, services_id);
            int n = 0;
            foreach (var time in times)
            {
                n++;
                string str = $"{n} - {time.start.TimeOfDay}";
                Console.WriteLine(str);
            }
            return n;
        }

        static int GetFreeDates(int filial_id, int worker_id)
        {
            
            clinic_service.Schedule[] schedules = methods.GetFreeDateSchedule(filial_id, worker_id);
            foreach (var schedule in schedules)
            {
                string str = $"{schedule.id} - {schedule.time1_start.Date}";
                Console.WriteLine(str);
            }
            return schedules.Length;
        }


        static int GetFilials()
        {
            
            clinic_service.Filial[] filials = methods.GetAllFilials();
            foreach (var filial in filials)
            {
                string filialstr = $"{filial.id} - {filial.address}";
                Console.WriteLine(filialstr);
            }
            return filials.Length;
        }

        static int GetServices()
        {
            
            clinic_service.Service[] services = methods.GetAllServices();
            foreach (var service in services)
            {
                string servicestr = $"{service.id} - {service.name} - {service.cost}";
                Console.WriteLine(servicestr);
            }
            return services.Length;
        }


        static int GetWorkersByService(int service_id)
        {
            
            clinic_service.Worker[] workers = methods.GetWorkerByService(service_id);
            foreach (var worker in workers)
            {
                string workerstr = $"{worker.id} - {worker.surname}  {worker.name}";
                Console.WriteLine(workerstr);
            }

            return workers.Length;
        }

        //PET
        static void MenuPet()
        {
            while (true)
            {
                Console.WriteLine("Мои питомцы");
                Console.WriteLine("1 - Показать всех питомцев");
                Console.WriteLine("2 - Показать приемы одного питомца");
                Console.WriteLine("3 - Добавить питомца");
                Console.WriteLine("-1 - Главное меню");

                string n = Console.ReadLine();
                try
                {
                    int num = Convert.ToInt32(n);
                    switch (num)
                    {
                        case -1:
                            Console.Clear();
                            Menu();
                            break;
                        case 1:
                            Console.Clear();
                            GetPets();
                            break;
                        case 2:
                            GetVisitByPet();
                            break;
                        case 3:
                            Console.Clear();
                            AddPet();
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Ошибка! Выберите пункт из меню");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Ошибка! Выберите пункт из меню");
                    continue;
                }
            }
        }
        
        static int GetPets()
        {
            
            clinic_service.Pet[] pets = methods.GetPetByClient(token);
            foreach (var pet in pets)
            {
                string petstr = $"{pet.id} - {pet.name} - {pet.kind.value}";
                Console.WriteLine(petstr);
            }
            return pets.Length;
        }

        static void AddPet()
        {
            Console.Clear();
            Console.WriteLine("Добавить питомца");
            Console.WriteLine("Введите имя питомца:");
            string name = Console.ReadLine();
            int kind_id = GetAllKind();
            bool sex = true;
            while (true)
            {
                Console.WriteLine("Пол питомца (0 - ж, 1 - м):");
                string s = Console.ReadLine();
                if (s == "-1")
                {
                    Console.Clear();
                    Menu();
                }
                if (s == "1" | s == "0")
                {
                    sex = Convert.ToBoolean(Convert.ToInt32(s));
                    break;
                }
                else
                {
                    Console.WriteLine("Ошибка! Выберите 0 или 1");
                    continue;
                }

            }
            
            int id = methods.AddPet(token, name, sex, kind_id);


        }

        static int GetAllKind()
        {
            while (true)
            {
                Console.WriteLine("(-1 - главное меню)");
                Console.WriteLine("Выберите вид питомца");
                
                clinic_service.Kind[] kinds = methods.GetAllKinds();
                foreach (var kind in kinds)
                {
                    Console.WriteLine($"{kind.id} - {kind.value}");
                }
                string n = Console.ReadLine();
                int num = 0;
                try
                {
                    num = Convert.ToInt32(n);
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка! Выберите номер из списка");
                    continue;
                }
                if (num == -1)
                {
                    Menu();
                }
                int c = 0;
                for (int i = 0; i < kinds.Length; i++)
                {
                    if (num == kinds[i].id)
                    {
                        c++;
                        break;
                    }
                }
                if (c > 0)
                {
                    return num;
                }
                else
                {
                    Console.WriteLine("Ошибка! Выберите номер из списка");
                    continue;
                }
            }
        }

        static void GetVisitByPet()
        {
            Console.Clear();
            int pet_id = 0;
            while (true)
            {
                Console.WriteLine("Выберите номер питомца");
                GetPets();
                string pet = Console.ReadLine();
                try
                {
                    pet_id = Convert.ToInt32(pet);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка!");
                }
            }

            clinic_service.Visit[] visits = methods.GetVisitByPet(token, pet_id);
            foreach (var visit in visits)
            {
                string visitstr = $"{visit.id} - {visit.date} - {visit.status} - {visit.cabinet} - {visit.filial.address} - {visit.pet.name} - {visit.duration} - {visit.doctor.surname} {visit.doctor.name}";
                Console.WriteLine(visitstr);
            }
        }



    }
}
