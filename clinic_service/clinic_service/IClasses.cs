﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using System.ServiceModel;

namespace clinic_service
{
    [ServiceContract]
    public interface IClasses
    {
        [OperationContract]
        int LoginVerification(string login);

        [OperationContract]
        string PassVerification(string login, string pwd);

        //SERVICE
        [OperationContract]
        List<Service> GetAllServices();

        [OperationContract]
        Service GetService(int id);

        //FILIAL
        [OperationContract]
        List<Filial> GetAllFilials();

        [OperationContract]
        Filial GetFilial(int id);

        //KIND
        [OperationContract]
        List<Kind> GetAllKinds();

        //CLIENT
        [OperationContract]
        int AddClient(string username, string pwd);

        [OperationContract]
        Client GetClientByUser(string token);

        [OperationContract]
        Client SaveClient(string token, string surname, string name, string patronymic, string phone, string mail);

        //PET
        [OperationContract]
        int AddPet(string token, string name, bool sex, int kind_id);

        [OperationContract]
        List<Pet> GetPetByClient(string token);

        [OperationContract]
        Pet GetPet(int id);

        //VISIT
        [OperationContract]
        List<Visit> GetVisitByClient(string token);

        [OperationContract]
        List<Visit> GetVisitByDate(DateTime date);

        [OperationContract]
        List<Visit> GetVisitByPet(string token, int pet_id);

        //WORKER
        [OperationContract]
        Worker GetWorker(int id);

        [OperationContract]
        List<Worker> GetWorkerByService(int service_id);

        //SCHEDULE
        [OperationContract]
        List<Schedule> GetFreeDateSchedule(int filial_id, int worker_id);

        [OperationContract]
        List<Times> GetFreeTimeSchedule(int schedule_id, params int[] services_id);

        [OperationContract]
        int AddVisit(string token, int doctor_id, int filial_id, DateTime date, int pet_id, params int[] services_id);


    }
}
