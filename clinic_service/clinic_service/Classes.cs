﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace clinic_service
{
   
    [DataContract]
    public class Service
    {
        [DataMember]
        public int id;
        [DataMember]
        public string name;
        [DataMember]
        public string description;
        [DataMember]
        public int duration;
        [DataMember]
        public double cost;
        [DataMember]
        public bool nurse;
        [DataMember]
        public bool visibility;
        [DataMember]
        public DateTime date_of_delete;
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public int id;
        [DataMember]
        public string password;
        [DataMember]
        public string username;
        [DataMember]
        public string ip;
        [DataMember]
        public string token;
        //[DataMember]
        //public Group groups;
    }


    [DataContract]
    public class Worker
    {
        [DataMember]
        public int id;
        [DataMember]
        public User user;
        [DataMember]
        public string position;
        [DataMember]
        public string surname;
        [DataMember]
        public string name;
        [DataMember]
        public string patronymic;
        [DataMember]
        public string phone;
        [DataMember]
        public string mail;
        //[DataMember]
        //public string photo;
        [DataMember]
        public DateTime date_of_birth;
        [DataMember]
        public string info;
        [DataMember]
        public bool visibility;
        [DataMember]
        public DateTime date_of_delete;
    }

    [DataContract]
    public class Filial
    {
        [DataMember]
        public int id;
        [DataMember]
        public string address_full;
        [DataMember]
        public string address;
        [DataMember]
        public string mail;
        [DataMember]
        public bool visibility;
        [DataMember]
        public DateTime date_of_delete;
    }

    [DataContract]
    public class Cabinet
    {
        [DataMember]
        public int id;
        [DataMember]
        public string name;
        [DataMember]
        public string description;
        [DataMember]
        public bool visibility;
        [DataMember]
        public DateTime date_of_delete;
    }

    [DataContract]
    public class Kind
    {
        [DataMember]
        public int id;
        [DataMember]
        public string value;
    }

    public class Client
    {
        [DataMember]
        public int id;
        [DataMember]
        public User user;
        [DataMember]
        public string surname;
        [DataMember]
        public string name;
        [DataMember]
        public string patronymic;
        [DataMember]
        public string phone;
        [DataMember]
        public string mail;
        //[DataMember]
        //public string photo;
        [DataMember]
        public DateTime date_of_birth;
        [DataMember]
        public DateTime date_of_delete;

    }

    public class Pet
    {
        [DataMember]
        public int id;
        [DataMember]
        public string name;
        [DataMember]
        public Client client;
        [DataMember]
        public string sex;
        [DataMember]
        public DateTime date_of_birth;
        [DataMember]
        public Kind kind;
        [DataMember]
        public DateTime date_of_delete;
    }

    public class Visit
    {
        [DataMember]
        public int id;
        [DataMember]
        public Client client;
        [DataMember]
        public DateTime date;
        [DataMember]
        public string comment;
        [DataMember]
        public double cost;
        [DataMember]
        public string status;
        [DataMember]
        public string cabinet;
        [DataMember]
        public Worker doctor;
        [DataMember]
        public Filial filial;
        [DataMember]
        public Pet pet;
        [DataMember]
        public DateTime date_of_delete;
        [DataMember]
        public int duration;
    }

    public class Schedule
    {
        [DataMember]
        public int id;
        [DataMember]
        public int filial_id;
        [DataMember]
        public int worker_id;
        [DataMember]
        public DateTime time1_start;
        [DataMember]
        public DateTime time1_end;

    }

    public class Times
    {
        [DataMember]
        public DateTime start;
        [DataMember]
        public DateTime end;
    }








    }
