﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;


using MySql.Data.MySqlClient;
using Org.BouncyCastle.Asn1.Cms;

namespace clinic_service
{
    
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Server : IClasses
    {
        static string connStr = ConfigurationManager.ConnectionStrings["DB"].ConnectionString;
        public int LoginVerification(string login)
        {
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = $"SELECT id FROM user WHERE username = @login";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();
            int id = 0;
            while (reader.Read())
            {
                id = Convert.ToInt32(reader[0].ToString());
            }
            conn.Close();
            return id;
        }

        public string PassVerification(string login, string pwd)
        {
            int id = 0;
            string token = "";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = $"SELECT id FROM user WHERE username = @login and password = @pwd";
            MySqlCommand cmd = new MySqlCommand();

            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@pwd", pwd);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                id = Convert.ToInt32(reader[0].ToString());
            }
            conn.Close();
            if (id > 0)
            {
                char[] sumols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                Random rand = new Random();

                for (int j = 1; j <= 30; j++)
                {
                    int sumols_num = rand.Next(0, sumols.Length - 1);
                    token += sumols[sumols_num];
                }
                SetToken(token, id);
            }

            return token;
        }

        private void SetToken(string token, int id)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = $"UPDATE user SET token = @token WHERE id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@token", token);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader2 = cmd.ExecuteReader();
            conn.Close();
        }
        //SERVICE
        public List<Service> GetAllServices()
        {
            List <Service> services = new List<Service>();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT * FROM service WHERE date_of_delete is null and visibility = True";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Service service = new Service();
                service.id = Convert.ToInt32(reader[0].ToString());
                service.name = Convert.ToString(reader[1].ToString());
                service.description = Convert.ToString(reader[2].ToString());
                service.duration = Convert.ToInt32(reader[3].ToString());
                service.cost = Convert.ToDouble(reader[4].ToString());
                service.nurse = Convert.ToBoolean(reader[7].ToString());

                services.Add(service);
            }
            conn.Close();
            return services;
        }

        public Service GetService(int id)
        {
            MySqlConnection conn = new MySqlConnection(connStr);

            Service service = new Service();

            conn.Open();
            string sql = "SELECT * FROM service WHERE date_of_delete is null and visibility = True and id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                service.id = Convert.ToInt32(reader[0].ToString());
                service.name = Convert.ToString(reader[1].ToString());
                service.description = Convert.ToString(reader[2].ToString());
                service.duration = Convert.ToInt32(reader[3].ToString());
                service.cost = Convert.ToDouble(reader[4].ToString());
                service.nurse = Convert.ToBoolean(reader[7].ToString());
            }
            conn.Close();
            return service;
        }

        //FILIAL
        public List<Filial> GetAllFilials()
        {
            List<Filial> filials = new List<Filial>();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT id, address, address_full FROM filial WHERE date_of_delete is null and visibility = True";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Filial filial = new Filial();
                filial.id = Convert.ToInt32(reader[0].ToString());
                filial.address = Convert.ToString(reader[1].ToString());
                filial.address_full = Convert.ToString(reader[2].ToString());

                filials.Add(filial);
            }
            conn.Close();
            return filials;
        }

        public Filial GetFilial(int id)
        {
            Filial filial = new Filial();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = $"SELECT id, address, address_full FROM filial WHERE date_of_delete is null and visibility = True and id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                filial.id = Convert.ToInt32(reader[0].ToString());
                filial.address = Convert.ToString(reader[1].ToString());
                filial.address_full = Convert.ToString(reader[2].ToString());
            }
            conn.Close();

            return filial;
        }

        //KIND
        public List<Kind> GetAllKinds()
        {
            List<Kind> kinds = new List<Kind>();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT * FROM kind";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Kind kind = new Kind();
                kind.id = Convert.ToInt32(reader[0].ToString());
                kind.value = Convert.ToString(reader[1].ToString());

                kinds.Add(kind);
            }
            conn.Close();
            return kinds;
        }

        private Kind GetKind(int id)
        {
            Kind kind = new Kind();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = $"SELECT * FROM kind WHERE id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                kind.id = Convert.ToInt32(reader[0].ToString());
                kind.value = Convert.ToString(reader[1].ToString());

            }
            conn.Close();
            return kind;
        }

        //CLIENT
        public int AddClient(string username, string pwd) //регистрация
        {
            int client_id = 0;
            int user_id = AddUser(username, pwd); 

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = $"INSERT INTO client (user_id) VALUES (@user_id)";
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("@user_id", user_id);
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            long last_id = cmd.LastInsertedId;
            conn.Close();
            client_id = Convert.ToInt32(last_id);
            return client_id;
        }

        public Client GetClientByUser(string token)
        {
            Client client = new Client();
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = "SELECT client.id, surname, name, patronymic, phone, mail, date_of_birth " +
                $"FROM client INNER JOIN user ON user.id = client.user_id WHERE token = @token";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@token", token);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                client.id = Convert.ToInt32(reader[0].ToString());
                client.surname = Convert.ToString(reader[1].ToString());
                client.name = Convert.ToString(reader[2].ToString());
                client.patronymic = Convert.ToString(reader[3].ToString());
                client.phone = Convert.ToString(reader[4].ToString());
                client.mail = Convert.ToString(reader[5].ToString());
            }
            conn.Close();
            return client;
        }

        public Client SaveClient(string token, string surname, string name, string patronymic, string phone, string mail)
        {
            if (token == null){ throw new Exception("Ошибка авторизации"); }

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            MySqlCommand cmd = conn.CreateCommand();

            string sql = "UPDATE client INNER JOIN user ON client.user_id = user.id SET";
            if(surname != null)
            {
                cmd.Parameters.AddWithValue("@surname", surname);
                sql = sql + $" surname = @surname,";
            }
            if(name != null)
            {
                cmd.Parameters.AddWithValue("@name", name);
                sql = sql + $" name = @name,";
            }
            if(patronymic != null){
                cmd.Parameters.AddWithValue("@patronymic", patronymic);
                sql = sql + $" patronymic = @patronymic,";
            }
            if(phone != null){
                cmd.Parameters.AddWithValue("@phone", phone);
                sql = sql + $" phone = @phone,";
            }
            if(mail != null){
                cmd.Parameters.AddWithValue("@mail", mail);
                sql = sql + $" mail = @mail,";
            }

            sql = sql.Substring(0, sql.Length - 1);
            cmd.Parameters.AddWithValue("@token", token);
            sql = sql + $" WHERE user.token = @token";

            
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            conn.Close();
            return GetClientByUser(token);
        }

        private Client GetClient(int id)
        {
            Client client = new Client();
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT id, surname, name, patronymic, phone, mail, date_of_birth" +
                $" FROM client WHERE id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                client.id = Convert.ToInt32(reader[0].ToString());
                client.surname = Convert.ToString(reader[1].ToString());
                client.name = Convert.ToString(reader[2].ToString());
                client.patronymic = Convert.ToString(reader[3].ToString());
                client.phone = Convert.ToString(reader[4].ToString());
                client.mail = Convert.ToString(reader[5].ToString());
            }
            conn.Close();

            return client;
        }


        //USER
        private int AddUser(string username, string pwd)
        {
            int id = 0;
            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = $"INSERT INTO user (username, password) VALUES (@username, @pwd)";
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@pwd", pwd);
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            long last_id = cmd.LastInsertedId;
            conn.Close();
            id = Convert.ToInt32(last_id);

            return id;
        }

        //PET
        public int AddPet(string token, string name, bool sex, int kind_id)
        {
            if (token == null) { throw new Exception("Ошибка авторизации"); }

            int client_id = GetClientByUser(token).id;

            string sql = "INSERT INTO pet (name, sex, kind_id, client_id) " +
                $"VALUES(@name, @sex, @kind_id, @client_id)"; 

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@sex", sex);
            cmd.Parameters.AddWithValue("@kind_id", kind_id);
            cmd.Parameters.AddWithValue("@client_id", client_id);
            conn.Open();
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            int id = Convert.ToInt32(cmd.LastInsertedId);
            conn.Close();
            return id;

        }

        public List<Pet> GetPetByClient(string token)
        {
            List<Pet> pets = new List<Pet>();
            Client client = GetClientByUser(token);
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT id, name, sex, kind_id" +
                $" FROM pet WHERE client_id = {client.id} and date_of_delete is null";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Pet pet = new Pet();
                pet.id = Convert.ToInt32(reader[0].ToString());
                pet.name = Convert.ToString(reader[1].ToString());
                if (Convert.ToBoolean(reader[2].ToString()))
                {
                    pet.sex = "м";
                }
                else
                {
                    pet.sex = "ж";
                }
                pet.kind = GetKind(Convert.ToInt32(reader[3].ToString()));
                pet.client = client;
                pets.Add(pet);
            }
            conn.Close();
            return pets;
        }

        public Pet GetPet(int id)
        {
            Pet pet = new Pet();
            string sql = $"SELECT id, name, sex, kind_id, client_id FROM pet WHERE id = @id";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                pet.id = Convert.ToInt32(reader[0].ToString());
                pet.name = Convert.ToString(reader[1].ToString());
                if (Convert.ToBoolean(reader[2].ToString()))
                {
                    pet.sex = "м";
                }
                else
                {
                    pet.sex = "ж";
                }
                pet.kind = GetKind(Convert.ToInt32(reader[3].ToString()));
                pet.client = GetClient(Convert.ToInt32(reader[4].ToString()));
            }
            conn.Close();
            return pet;
        }

        //VISIT
        public List<Visit> GetVisitByClient(string token)
        {
            if (token == null) { throw new Exception("Ошибка авторизации"); }
            Client client = GetClientByUser(token);
            List<Visit> visits = new List<Visit>();

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT v.id, v.date, v.comment, v.cost, c.name, v.doctor_id, v.filial_id, v.pet_id, v.duration, v.status" +
                $" FROM visit v LEFT JOIN cabinet c ON c.id = v.cabinet_id WHERE v.client_id = @client_id and v.date_of_delete is null";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@client_id", client.id);

            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Visit visit = new Visit();
                visit.id = Convert.ToInt32(reader[0].ToString());
                visit.date = Convert.ToDateTime(reader[1].ToString());
                visit.comment = Convert.ToString(reader[2].ToString());
                visit.cost = Convert.ToDouble(reader[3].ToString());
                visit.cabinet = Convert.ToString(reader[4].ToString());
                visit.doctor = GetWorker(Convert.ToInt32(reader[5].ToString()));
                visit.filial = GetFilial(Convert.ToInt32(reader[6].ToString()));
                visit.pet = GetPet(Convert.ToInt32(reader[7].ToString()));
                visit.duration = Convert.ToInt32(reader[8].ToString());
                int status = Convert.ToInt32(reader[9].ToString());
                switch (status)
                {
                    case 0:
                        visit.status = "актуальный";
                        break;
                    case 1:
                        visit.status = "выполнен";
                        break;
                    case 2:
                        visit.status = "отменен";
                        break;
                }

                visits.Add(visit);
            }
            conn.Close();

            return visits;
        }

        public List<Visit> GetVisitByPet(string token, int pet_id)
        {
            if (token == null) { throw new Exception("Ошибка авторизации"); }
            Client client = GetClientByUser(token);
            List<Visit> visits = new List<Visit>();

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT v.id, v.date, v.comment, v.cost, c.name, v.doctor_id, v.filial_id, v.pet_id, v.duration, v.status" +
                $" FROM visit v LEFT JOIN cabinet c ON c.id = v.cabinet_id WHERE v.client_id = @client_id and pet_id = @pet_id and v.date_of_delete is null";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@client_id", client.id);
            cmd.Parameters.AddWithValue("@pet_id", pet_id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Visit visit = new Visit();
                visit.id = Convert.ToInt32(reader[0].ToString());
                visit.date = Convert.ToDateTime(reader[1].ToString());
                visit.comment = Convert.ToString(reader[2].ToString());
                visit.cost = Convert.ToDouble(reader[3].ToString());
                visit.cabinet = Convert.ToString(reader[4].ToString());
                visit.doctor = GetWorker(Convert.ToInt32(reader[5].ToString()));
                visit.filial = GetFilial(Convert.ToInt32(reader[6].ToString()));
                visit.pet = GetPet(Convert.ToInt32(reader[7].ToString()));
                visit.duration = Convert.ToInt32(reader[8].ToString());
                int status = Convert.ToInt32(reader[9].ToString());
                switch (status)
                {
                    case 0:
                        visit.status = "актуальный";
                        break;
                    case 1:
                        visit.status = "выполнен";
                        break;
                    case 2:
                        visit.status = "отменен";
                        break;
                }

                visits.Add(visit);
            }
            conn.Close();

            return visits;
        }


        public List<Visit> GetVisitByDate(DateTime date)
        {
            List<Visit> visits = new List<Visit>();
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = $"SELECT id, duration, date, doctor_id FROM visit WHERE date(date) = @date ORDER BY date ASC ";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@date", date.ToString("yyyy-MM-dd"));
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Visit visit = new Visit();
                visit.id = Convert.ToInt32(reader[0].ToString());
                visit.duration = Convert.ToInt32(reader[1].ToString());
                visit.date = Convert.ToDateTime(reader[2].ToString());
                visit.doctor = GetWorker(Convert.ToInt32(reader[3].ToString()));
                visits.Add(visit);
            }
            conn.Close();

            return visits;
        }


        public int AddVisit(string token, int doctor_id, int filial_id, DateTime date, int pet_id, params int[] services_id)
        {
            if (token == null) { throw new Exception("Ошибка авторизации"); }
            int id = 0;
            int duration = 0;
            double cost = 0;
            for (int x = 0; x < services_id.Length; x++)
            {
                Service service = GetService(services_id[x]);
                duration = duration + service.duration;
                cost = cost + service.cost;
            }

            Client client = GetClientByUser(token);
            if (client.id == 0) { throw new Exception("Ошибка авторизации"); }

            MySqlConnection conn = new MySqlConnection(connStr);

            conn.Open();
            string sql = $"INSERT INTO visit (date, cost, filial_id, doctor_id, pet_id, client_id, status, duration, cabinet_id) " +
                $"VALUES(@date, @cost, @filial_id, @doctor_id, @pet_id, @client_id, 0, @duration, 1)";
            string date1 = date.ToString("yyyy-MM-dd");
            MySqlCommand cmd = conn.CreateCommand();
            cmd.Parameters.AddWithValue("@date", date.ToString("yyyy-MM-dd hh:mm:ss"));
            cmd.Parameters.AddWithValue("@cost", cost);
            cmd.Parameters.AddWithValue("@filial_id", filial_id);
            cmd.Parameters.AddWithValue("@doctor_id", doctor_id);
            cmd.Parameters.AddWithValue("@pet_id", pet_id);
            cmd.Parameters.AddWithValue("@client_id", client.id);
            cmd.Parameters.AddWithValue("@duration", duration);
            cmd.CommandText = sql;
            cmd.ExecuteNonQuery();
            long last_id = cmd.LastInsertedId;
            conn.Close();
            id = Convert.ToInt32(last_id);

            return id;
        }

        //WORKER
        public Worker GetWorker(int id)
        {
            Worker worker = new Worker();
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT w.id, w.surname, w.name, w.patronymic, w.phone, w.mail, p.name" +
                $" FROM worker w LEFT JOIN  position p ON w.position_id = p.id WHERE w.id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                worker.id = Convert.ToInt32(reader[0].ToString());
                worker.surname = Convert.ToString(reader[1].ToString());
                worker.name = Convert.ToString(reader[2].ToString());
                worker.patronymic = Convert.ToString(reader[3].ToString());
                worker.phone = Convert.ToString(reader[4].ToString());
                worker.mail = Convert.ToString(reader[5].ToString());
                worker.position = Convert.ToString(reader[6].ToString());
            }
            conn.Close();

            return worker;
        }

        public List<Worker> GetWorkerByService(int service_id)
        {
            List<Worker> workers = new List<Worker>();
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT w.id, w.surname, w.name, w.patronymic, w.phone, w.mail, p.name" +
                $" FROM worker w INNER JOIN worker_services ON w.id = worker_services.worker_id " +
                $"LEFT JOIN  position p ON w.position_id = p.id " +
                $"WHERE date_of_delete is null and visibility = True and service_id = @service_id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@service_id", service_id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Worker worker = new Worker();
                worker.id = Convert.ToInt32(reader[0].ToString());
                worker.surname = Convert.ToString(reader[1].ToString());
                worker.name = Convert.ToString(reader[2].ToString());
                worker.patronymic = Convert.ToString(reader[3].ToString());
                worker.phone = Convert.ToString(reader[4].ToString());
                worker.mail = Convert.ToString(reader[5].ToString());
                worker.position = Convert.ToString(reader[6].ToString());
                workers.Add(worker);
            }
            conn.Close();

            return workers;

        }

        //SCHEDULE
        public List<Schedule> GetFreeDateSchedule(int filial_id, int worker_id)
        {
            List<Schedule> dates = new List<Schedule>();
            MySqlConnection conn = new MySqlConnection(connStr);
            DateTime today = DateTime.Today;
            string todaystr = today.ToString("yyyy-MM-dd");
            conn.Open();
            string sql = $"SELECT id, time1_start FROM schedule WHERE worker_id = @worker_id and filial_id = @filial_id and date(time1_start) >= '{todaystr}'";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@worker_id", worker_id);
            cmd.Parameters.AddWithValue("@filial_id", filial_id);

            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Schedule schedule = new Schedule();
                schedule.id = Convert.ToInt32(reader[0].ToString());
                schedule.time1_start = Convert.ToDateTime(reader[1].ToString());
                dates.Add(schedule);
            }
            conn.Close();

            return dates;
        }

        public List<Times> GetFreeTimeSchedule(int schedule_id, params int[] services_id) 
        {
            int duration = 0;
            for (int x = 0; x < services_id.Length; x++)
            {
                Service service = GetService(services_id[x]);
                duration = duration + service.duration;
            }

            Schedule sel_schedule = GetSchedule(schedule_id);
            DateTime dt = sel_schedule.time1_start;

            List<Visit> visits = GetVisitByDate(dt);

            List<Times> times = new List<Times>();

            foreach (var visit in visits)
            {
                if (visit.doctor.id == sel_schedule.worker_id)
                {
                    while (true)
                    {
                        if (dt >= visit.date && dt <= visit.date.AddMinutes(visit.duration))
                        {
                            dt = visit.date.AddMinutes(visit.duration).AddMinutes(5 + duration);
                            break;
                        }
                        else
                        {
                            Times time = new Times();
                            time.end = dt;
                            time.start = dt.AddMinutes(-duration);
                            times.Add(time);
                            dt = dt.AddMinutes(5);
                        }
                    }
                }
            }

            while (dt <= sel_schedule.time1_end)
            {
                dt = dt.AddMinutes(5 + duration);
                if (dt <= sel_schedule.time1_end)
                {
                    Times time2 = new Times();
                    time2.end = dt;
                    time2.start = dt.AddMinutes(-duration);
                    times.Add(time2);
                }
            }
            return times;
        }

        private Schedule GetSchedule(int id)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            Schedule schedule = new Schedule();
            conn.Open();
            string sql = $"SELECT id, time1_start, time1_end, worker_id FROM schedule WHERE id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Connection = conn;
            cmd.CommandText = sql;
            DbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                schedule.id = Convert.ToInt32(reader[0].ToString());
                schedule.time1_start = Convert.ToDateTime(reader[1].ToString());
                schedule.time1_end = Convert.ToDateTime(reader[2].ToString());
                schedule.worker_id = Convert.ToInt32(reader[3].ToString());
            }
            conn.Close();

            return schedule;
        }

        





    }
}
