﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using clinic_service;
using System.ServiceModel;

namespace clinic_host
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(typeof(clinic_service.Server), new Uri("http://localhost:8733/Design_Time_Addresses/clinic_service/Service1/"));
            host.AddServiceEndpoint(typeof(clinic_service.IClasses), new BasicHttpBinding(), "");
            host.Open();
            Console.WriteLine("Служба запущена");
            Console.ReadLine();
            host.Close();
        }
    }
}
